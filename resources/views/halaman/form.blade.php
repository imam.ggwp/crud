@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection


@section('content')
		<h1>Buat Account Baru</h1>
		<h2>Sign Up Form</h2>
		<form action="/welcome" method="post">
            @csrf
		    <label>First Name :</label><br><br> 
		    <input type="text" name="fnama"> <br> <br>
		    <label>Last Name :</label><br><br> 
		    <input type="text" name="lnama"> <br> <br>
		    <label>Gender :</label> <br>
		    <input type="radio" name="gender" value="m"> Male
		    <br> 
		    <input type="radio" name="gender" value="f"> Female
		    <br> 
		    <input type="radio" name="gender" value="o"> Other
		    <br> <br>
		    <label>Nationality</label> <br><br> 
		    <select name="Nationality">
			    <option value="1">Indonesia</option>
			    <option value="2">Ireland</option>
			    <option value="3">USA</option>
		</select> <br> <br>
		<label>Language Spoken</label> <br><br> 
		<input type="checkbox" name="language"> Bahasa Indonesia <br>
		<input type="checkbox" name="language"> English <br>
		<input type="checkbox" name="language"> Other <br><br> 
		<br> 
		
		<label> Bio </label> <br><br>
		<textarea name="Bio" cols="30" rows="10"></textarea> <br> <br>
		<input type="submit" value="Sign Up"> 
		</form>
@endsection